/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 David Mosberger-Tang and Tristan Tarrant
   This file is part of the SANE package.

   SANE is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   SANE is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with sane; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */
/*
  The preview strategy is as follows:

   1) A preview always acquires an image that covers the entire
      scan surface.  This is necessary so the user can see not
      only what is, but also what isn't selected.

   2) The preview must be zoomable so the user can precisely pick
      the selection area even for small scans on a large scan
      surface.  The user also should have the option of resizing
      the preview window.

   3) We let the user/backend pick whether a preview is in color,
      grayscale, lineart or what not.  The only options that the
      preview may (temporarily) modify are:

	- resolution (set so the preview fills the window)
	- scan area options (top-left corner, bottom-right corner)
	- preview option (to let the backend know we're doing a preview)

   4) The size of the scan surface is determined based on the constraints
      of the four corner coordinates.  Missing constraints are replaced
      by +/-INF as appropriate (-INF form top-left, +INF for bottom-right
      coords).

   5) The size of the preview window is determined based on the scan
      surface size:

          If the surface area is specified in pixels and if that size
	  fits on the screen, we use that size.  In all other cases,
	  we make the window of a size so that neither the width nor
	  the height is bigger than some fraction of the screen-size
	  while preserving the aspect ratio (a surface width or height
	  of INF implies an aspect ratio of 1).

   6) Given the preview window size and the scan surface size, we
      select the resolution so the acquired preview image just fits
      in the preview window.  The resulting resolution may be out
      of range in which case we pick the minum/maximum if there is
      a range or word-list constraint or a default value if there is
      no such constraint.

   7) Once a preview image has been acquired, we know the size of the
      preview image (in pixels).  An initial scale factor is chosen
      so the image fits into the preview window.

      */

#include "../include/sane/config.h"

#include <assert.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>
#include <sys/param.h>

#include "gtkglue.h"
#include "preview.h"
#include "preferences.h"
#include "progress.h"

#define DBG_fatal 0
#define DBG_error 1
#define DBG_warning 2
#define DBG_info 3
#define DBG_debug 4
#define DBG_proc      5
#define DBG_proc2     50
#define DBG_proc3     100

#define BACKEND_NAME preview
#include "../include/sane/sanei_debug.h"

#ifndef PATH_MAX
# define PATH_MAX	1024
#endif

/* Anything bigger than 2G will do, since SANE coordinates are 32 bit
   values.  */
#define INF		5.0e9

#define MM_PER_INCH	25.4

/* Cut fp conversion routines some slack: */
#define GROSSLY_DIFFERENT(f1,f2)	(fabs ((f1) - (f2)) > 1e-3)

#ifdef __alpha__
  /* This seems to be necessary for at least some XFree86 3.1.2
     servers.  It's known to be necessary for the XF86_TGA server for
     Linux/Alpha.  Fortunately, it's no great loss so we turn this on
     by default for now.  */
# define XSERVER_WITH_BUGGY_VISUALS
#endif

/* forward declarations */
static void scan_start (Preview * p);
static void scan_done (Preview * p);

static gboolean
_last_call_time_check(unsigned long interval)
{
    struct timeval t;
    unsigned long utime;
    static unsigned long last_call = 0;

    gettimeofday(&t,NULL);
    utime = 1000000 * t.tv_sec + t.tv_usec;

    if ((utime - last_call) < interval)
        return FALSE;

    last_call = utime;

    return TRUE;
}


static void
screen_size_get_dimensions (gint *width, gint *height)
{
    GdkScreen *screen;
    GdkWindow *window;
    gint scale;

    screen = gdk_screen_get_default ();
    window = gdk_screen_get_root_window (screen);
    
    scale = gdk_window_get_scale_factor (window);

    *width = gdk_window_get_width (window) / scale;
    *height = gdk_window_get_height (window) / scale;
}

static void
draw_rect(cairo_t *cr, double coord[4])
{
  double x, y, w, h;

  x = coord[0];
  y = coord[1];
  w = coord[2] - x;
  h = coord[3] - y;
  if (w < 0)
    {
      x = coord[2];
      w = -w;
    }
  if (h < 0)
    {
      y = coord[3];
      h = -h;
    }
  cairo_set_line_width(cr, 1.5);
  const double dashes1[2] = {4.0, 4.0};
  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_set_dash(cr, dashes1, sizeof(dashes1)/sizeof(dashes1[0]), 0);
  cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_rectangle(cr, x, y, w + 1, h + 1);
  cairo_stroke(cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
  const double dashes2[4] = {0.0, 4.0, 4.0, 0.0};
  cairo_set_dash(cr, dashes2, sizeof(dashes2)/sizeof(dashes2[0]), 0);
  cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
  cairo_rectangle(cr, x, y, w + 1, h + 1);
  cairo_stroke(cr);
}

static void
draw_selection (Preview * p, cairo_t *cr)
{
  if (p->previous_selection.active)
    draw_rect (cr, p->previous_selection.coord);

  if (p->selection.active)
    draw_rect (cr, p->selection.coord);

  p->previous_selection = p->selection;
}

static void
update_selection (Preview * p)
{
  float min, max, normal, dev_selection[4];
  const SANE_Option_Descriptor *opt;
  SANE_Status status;
  SANE_Word val;
  int i, optnum;

  p->previous_selection = p->selection;

  memcpy (dev_selection, p->surface, sizeof (dev_selection));
  for (i = 0; i < 4; ++i)
    {
      optnum = p->dialog->well_known.coord[i];
      if (optnum > 0)
	{
	  opt = sane_get_option_descriptor (p->dialog->dev, optnum);
	  status = sane_control_option (p->dialog->dev, optnum,
					SANE_ACTION_GET_VALUE, &val, 0);
	  if (status != SANE_STATUS_GOOD)
	    continue;
	  if (opt->type == SANE_TYPE_FIXED)
	    dev_selection[i] = SANE_UNFIX (val);
	  else
	    dev_selection[i] = val;
	}
    }
  for (i = 0; i < 2; ++i)
    {
      min = p->surface[i];
      if (min <= -INF)
	min = 0.0;
      max = p->surface[i + 2];
      if (max >= INF)
	max = p->preview_width;

      normal = ((i == 0) ? p->preview_width : p->preview_height) - 1;
      normal /= (max - min);
      p->selection.active = TRUE;
      p->selection.coord[i] = ((dev_selection[i] - min) * normal) + 0.5;
      p->selection.coord[i + 2] =
	((dev_selection[i + 2] - min) * normal) + 0.5;
      if (p->selection.coord[i + 2] < p->selection.coord[i])
	p->selection.coord[i + 2] = p->selection.coord[i];
    }
}

static void
get_image_scale (Preview * p, float *xscalep, float *yscalep)
{
  float xscale, yscale;

  if (p->image_width == 0)
    xscale = 1.0;
  else
    {
      xscale = p->image_width / (float) p->preview_width;
      if (p->image_height > 0 && p->preview_height * xscale < p->image_height)
	xscale = p->image_height / (float) p->preview_height;
    }
  yscale = xscale;

  if (p->surface_unit == SANE_UNIT_PIXEL
      && p->image_width <= p->preview_width
      && p->image_height <= p->preview_height)
    {
      float swidth, sheight;

      assert (p->surface_type == SANE_TYPE_INT);
      swidth = (p->surface[GSG_BR_X] - p->surface[GSG_TL_X] + 1);
      sheight = (p->surface[GSG_BR_Y] - p->surface[GSG_TL_Y] + 1);
      xscale = 1.0;
      yscale = 1.0;
      if (p->image_width > 0 && swidth < INF)
	xscale = p->image_width / swidth;
      if (p->image_height > 0 && sheight < INF)
	yscale = p->image_height / sheight;
    }
  *xscalep = xscale;
  *yscalep = yscale;
}

static void
paint_image (Preview * p)
{
  float xscale, yscale, src_x, src_y;
  int dst_x, dst_y, height, x, y, src_offset;
  gint gwidth, gheight;

  gwidth = p->preview_width;
  gheight = p->preview_height;

  get_image_scale (p, &xscale, &yscale);

  if (p->preview_row == NULL)
     p->preview_row = malloc (3 * gwidth);
  else
     p->preview_row = realloc (p->preview_row, 3 * gwidth);
  memset (p->preview_row, 0xff, 3 * gwidth);
  if (p->preview_data == NULL)
     p->preview_data = malloc (3 * gwidth * gheight);
  else
     p->preview_data = realloc (p->preview_data, 3 * gwidth * gheight);
  memset (p->preview_data, 0xff, 3 * gwidth * gheight);
  gtk_widget_queue_draw (p->window);

  /* don't draw last line unless it's complete: */
  height = p->image_y;
  if (p->image_x == 0 && height < p->image_height)
    ++height;

  /* for now, use simple nearest-neighbor interpolation: */
  src_offset = 0;
  src_x = src_y = 0.0;
  for (dst_y = 0; dst_y < gheight; ++dst_y)
    {
      y = (int) (src_y + 0.5);
      if (y >= height)
	break;
      src_offset = y * 3 * p->image_width;

      if (p->image_data)
	for (dst_x = 0; dst_x < gwidth; ++dst_x)
	  {
	    x = (int) (src_x + 0.5);
	    if (x >= p->image_width)
	      break;

	    p->preview_row[3 * dst_x + 0] =
	      p->image_data[src_offset + 3 * x + 0];
	    p->preview_row[3 * dst_x + 1] =
	      p->image_data[src_offset + 3 * x + 1];
	    p->preview_row[3 * dst_x + 2] =
	      p->image_data[src_offset + 3 * x + 2];
	    src_x += xscale;
	  }
	  memcpy(p->preview_data + (size_t) dst_y * (size_t) gwidth * 3, p->preview_row, (size_t) gwidth * 3);
      src_x = 0.0;
      src_y += yscale;
    }
  gtk_widget_queue_draw (p->window);
}

static void
display_partial_image (Preview * p)
{
  DBG(DBG_proc, "preview_display_partial_image\n");

  paint_image(p);
}

static void
display_maybe (Preview * p)
{
  time_t now;

  time (&now);
  if (now > p->image_last_time_updated)
    {
      p->image_last_time_updated = now;
      display_partial_image (p);
    }
}

static void
display_image (Preview * p)
{
  if (p->params.lines <= 0 && p->image_y < p->image_height)
    {
      p->image_height = p->image_y;
      p->image_data = realloc (p->image_data,
			       3 * p->image_width * p->image_height);
      assert (p->image_data);
      p->preview_data = realloc (p->preview_data,
			       3 * p->image_width * p->image_height);
    }
  display_partial_image (p);
  scan_done (p);
}

static void
preview_area_resize (GtkWidget * widget, GdkEventConfigure *event, gpointer data)
{
  float min_x, max_x, min_y, max_y, xscale, yscale, f;
  GtkAllocation alloc;
  Preview *p = data;

  /*if (!_last_call_time_check(1000000))
    return;
*/
  gtk_widget_get_allocated_size (widget, &alloc, NULL);

  p->preview_width = alloc.width;
  p->preview_height = alloc.height;

  if (p->preview_row)
    p->preview_row = realloc (p->preview_row, 3 * p->preview_width);
  else
    p->preview_row = malloc (3 * p->preview_width);

  /* set the ruler ranges: */

  min_x = p->surface[GSG_TL_X];
  if (min_x <= -INF)
    min_x = 0.0;

  max_x = p->surface[GSG_BR_X];
  if (max_x >= INF)
    max_x = p->preview_width - 1;

  min_y = p->surface[GSG_TL_Y];
  if (min_y <= -INF)
    min_y = 0.0;

  max_y = p->surface[GSG_BR_Y];
  if (max_y >= INF)
    max_y = p->preview_height - 1;

  /* convert mm to inches if that's what the user wants: */

  if (p->surface_unit == SANE_UNIT_MM)
    {
      double factor = 1.0 / preferences.length_unit;
      min_x *= factor;
      max_x *= factor;
      min_y *= factor;
      max_y *= factor;
    }

  get_image_scale (p, &xscale, &yscale);

  if (p->surface_unit == SANE_UNIT_PIXEL)
    f = 1.0 / xscale;
  else if (p->image_width)
    f = xscale * p->preview_width / p->image_width;
  else
    f = 1.0;

  gtk3_ruler_set_range (GTK3_RULER (p->hruler),
		             f * min_x,
			     f * max_x,
			     // f * min_x,
		             20); // max_size

  if (p->surface_unit == SANE_UNIT_PIXEL)
    f = 1.0 / yscale;
  else if (p->image_height)
    f = yscale * p->preview_height / p->image_height;
  else
    f = 1.0;

  gtk3_ruler_set_range (GTK3_RULER (p->vruler),
		       f * min_y,
		       f * max_y,
		       // f * min_y,
		       20); // max_size

  update_selection (p);
  paint_image (p);
  //gtk_widget_queue_draw (p->window);
}

static void
get_bounds (const SANE_Option_Descriptor * opt, float *minp, float *maxp)
{
  float min, max;
  int i;

  min = -INF;
  max = INF;
  switch (opt->constraint_type)
    {
    case SANE_CONSTRAINT_RANGE:
      min = opt->constraint.range->min;
      max = opt->constraint.range->max;
      break;

    case SANE_CONSTRAINT_WORD_LIST:
      min = INF;
      max = -INF;
      for (i = 1; i <= opt->constraint.word_list[0]; ++i)
	{
	  if (opt->constraint.word_list[i] < min)
	    min = opt->constraint.word_list[i];
	  if (opt->constraint.word_list[i] > max)
	    max = opt->constraint.word_list[i];
	}
      break;

    default:
      break;
    }
  if (opt->type == SANE_TYPE_FIXED)
    {
      if (min > -INF && min < INF)
	min = SANE_UNFIX (min);
      if (max > -INF && max < INF)
	max = SANE_UNFIX (max);
    }
  *minp = min;
  *maxp = max;
}

static void
save_option (Preview * p, int option, SANE_Word * save_loc, int *valid)
{
  SANE_Status status;

  if (option <= 0)
    {
      *valid = 0;
      return;
    }
  status = sane_control_option (p->dialog->dev, option, SANE_ACTION_GET_VALUE,
				save_loc, 0);
  *valid = (status == SANE_STATUS_GOOD);
}

static void
restore_option (Preview * p, int option, SANE_Word saved_value, int valid)
{
  const SANE_Option_Descriptor *opt;
  SANE_Status status;
  SANE_Handle dev;

  if (!valid)
    return;

  dev = p->dialog->dev;
  status = sane_control_option (dev, option, SANE_ACTION_SET_VALUE,
				&saved_value, 0);
  if (status != SANE_STATUS_GOOD)
    {
      char buf[256];
      opt = sane_get_option_descriptor (dev, option);
      snprintf (buf, sizeof (buf), "Failed restore value of option %s: %s.",
		opt->name, sane_strstatus (status));
      gsg_error (buf);
    }
}

static SANE_Status
set_option_float (Preview * p, int option, float value)
{
  const SANE_Option_Descriptor *opt;
  SANE_Handle dev;
  SANE_Word word;
  SANE_Status status;

  if (option <= 0 || value <= -INF || value >= INF)
    return SANE_STATUS_INVAL;

  dev = p->dialog->dev;
  opt = sane_get_option_descriptor (dev, option);
  if (opt->type == SANE_TYPE_FIXED)
    word = SANE_FIX (value) + 0.5;
  else
    word = value + 0.5;
  status = sane_control_option (dev, option, SANE_ACTION_SET_VALUE, &word, 0);
  if (status != SANE_STATUS_GOOD)
    {
      char buf[256];
      opt = sane_get_option_descriptor (dev, option);
      snprintf (buf, sizeof (buf), "Failed to set option %s: %s.",
		opt->name, sane_strstatus (status));
      gsg_error (buf);
      return status;
    }
  return SANE_STATUS_GOOD;
}

static void
set_option_bool (Preview * p, int option, SANE_Bool value)
{
  SANE_Handle dev;
  SANE_Status status;

  if (option <= 0)
    return;

  dev = p->dialog->dev;
  status =
    sane_control_option (dev, option, SANE_ACTION_SET_VALUE, &value, 0);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (DBG_fatal, "set_option_bool: sane_control_option failed: %s\n",
	   sane_strstatus (status));
      return;
    }
}

static int
increment_image_y (Preview * p)
{
  size_t extra_size, offset;
  char buf[256];

  p->image_x = 0;
  ++p->image_y;
  if (p->params.lines <= 0 && p->image_y >= p->image_height)
    {
      offset = 3 * p->image_width * p->image_height;
      extra_size = 3 * 32 * p->image_width;
      p->image_height += 32;
      p->image_data = realloc (p->image_data, offset + extra_size);
      p->preview_data = realloc (p->preview_data, offset + extra_size);
      if (!p->image_data)
	{
	  snprintf (buf, sizeof (buf),
		    "Failed to reallocate image memory: %s.",
		    strerror (errno));
	  gsg_error (buf);
	  scan_done (p);
	  return -1;
	}
      memset (p->image_data + offset, 0xff, extra_size);
      memset (p->preview_data + offset, 0xff, extra_size);
    }
  return 0;
}

static gboolean
input_available (gint source, GIOCondition cond, gpointer data)
{
  SANE_Status status;
  Preview *p = data;
  u_char buf[8192];
  SANE_Handle dev;
  SANE_Int len;
  int i, j;

  DBG_INIT ();

  DBG (DBG_debug, "input_available: enter\n");

  dev = p->dialog->dev;
  while (1)
    {
      status = sane_read (dev, buf, sizeof (buf), &len);
      if (status != SANE_STATUS_GOOD)
	{
	  if (status == SANE_STATUS_EOF)
	    {
	      if (p->params.last_frame)
		display_image (p);
	      else
		{
		  if (p->input_tag < 0)
		    {
		      display_maybe (p);
		      while (gtk_events_pending ())
		      {
		         gtk_widget_queue_draw(p->window);
			     gtk_main_iteration ();
			  }
		    }
		  else
		    {
		      g_source_remove (p->input_tag);
		      p->input_tag = -1;
		    }
		  scan_start (p);
		  break;
		}
	    }
	  else
	    {
	      char errbuf[100];
	      snprintf (errbuf, sizeof (errbuf), "Error during read: %s.",
			sane_strstatus (status));
	      gsg_error (errbuf);
	    }
	  scan_done (p);
	  return TRUE;
	}
      if (!len)			/* out of data for now */
	{
	  if (p->input_tag < 0)
	    {
	      display_maybe (p);
	      while (gtk_events_pending ())
		      {
		         gtk_widget_queue_draw(p->window);
			     gtk_main_iteration ();
			  }
	      continue;
	    }
	  else
	    break;
	}

      switch (p->params.format)
	{
	case SANE_FRAME_RGB:
	  switch (p->params.depth)
	    {
	    case 1:
	      for (i = 0; i < len; ++i)
		{
		  u_char mask = buf[i];

		  for (j = 7; j >= 0; --j)
		    {
		      u_char gl = (mask & (1 << j)) ? 0xff : 0x00;

		      p->image_data[p->image_offset] = gl;
		      if (j != 0)
			p->image_offset += 3;
		      else
			{
			  if ((p->image_offset % 3) != 2)
			    p->image_offset -= 20;
			  else
			    p->image_offset++;
			}
		      if (p->image_offset % 3 == 0)
			{
			  if (++p->image_x >= p->image_width)
			    {
			      if (increment_image_y (p) < 0)
				return FALSE;
			    }
			}
		    }
		}
	      break;

	    case 8:
	      for (i = 0; i < len; ++i)
		{
		  p->image_data[p->image_offset++] = buf[i];
		  if (p->image_offset % 3 == 0)
		    {
		      if (++p->image_x >= p->image_width
			  && increment_image_y (p) < 0)
			return FALSE;
		    }
		}
	      break;
	    case 16:
	      for (i = 0; i < len; ++i)
		{
		  guint16 value = buf[i];
		  if ((i % 2) == 1)
		    p->image_data[p->image_offset++] = *(guint8 *) (&value);
		  if (p->image_offset % 6 == 0)
		    {
		      if (++p->image_x >= p->image_width
			  && increment_image_y (p) < 0)
			return FALSE;
		    }
		}
	      break;
	    default:
	      goto bad_depth;
	    }
	  break;

	case SANE_FRAME_GRAY:
	  switch (p->params.depth)
	    {
	    case 1:
	      for (i = 0; i < len; ++i)
		{
		  u_char mask = buf[i];

		  for (j = 7; j >= 0; --j)
		    {
		      u_char gl = (mask & (1 << j)) ? 0x00 : 0xff;
		      p->image_data[p->image_offset++] = gl;
		      p->image_data[p->image_offset++] = gl;
		      p->image_data[p->image_offset++] = gl;
		      if (++p->image_x >= p->image_width)
			{
			  if (increment_image_y (p) < 0)
			    return FALSE;
			  break;	/* skip padding bits */
			}
		    }
		}
	      break;

	    case 8:
	      for (i = 0; i < len; ++i)
		{
		  u_char gl = buf[i];
		  p->image_data[p->image_offset++] = gl;
		  p->image_data[p->image_offset++] = gl;
		  p->image_data[p->image_offset++] = gl;
		  if (++p->image_x >= p->image_width
		      && increment_image_y (p) < 0)
		    return FALSE;
		}
	      break;
	    case 16:
	      for (i = 0; i < len; ++i)
		{
		  guint16 value = buf[i];
		  if ((i % 2) == 1)
		    {
		      p->image_data[p->image_offset++] = *(guint8 *) (&value);
		      p->image_data[p->image_offset++] = *(guint8 *) (&value);
		      p->image_data[p->image_offset++] = *(guint8 *) (&value);
		    }
		  if (p->image_offset % 2 == 0)
		    {
		      if (++p->image_x >= p->image_width
			  && increment_image_y (p) < 0)
			return FALSE;
		    }
		}
	      break;

	    default:
	      goto bad_depth;
	    }
	  break;

	case SANE_FRAME_RED:
	case SANE_FRAME_GREEN:
	case SANE_FRAME_BLUE:
	  switch (p->params.depth)
	    {
	    case 1:
	      for (i = 0; i < len; ++i)
		{
		  u_char mask = buf[i];

		  for (j = 7; j >= 0; --j)
		    {
		      u_char gl = (mask & (1 << j)) ? 0xff : 0x00;
		      p->image_data[p->image_offset] = gl;
		      p->image_offset += 3;
		      if (++p->image_x >= p->image_width
			  && increment_image_y (p) < 0)
			return FALSE;
		    }
		}
	      break;

	    case 8:
	      for (i = 0; i < len; ++i)
		{
		  p->image_data[p->image_offset] = buf[i];
		  p->image_offset += 3;
		  if (++p->image_x >= p->image_width
		      && increment_image_y (p) < 0)
		    return FALSE;
		}
	      break;

	    case 16:
	      for (i = 0; i < len; ++i)
		{
		  guint16 value = buf[i];
		  if ((i % 2) == 1)
		    {
		      p->image_data[p->image_offset] = *(guint8 *) (&value);
		      p->image_offset += 3;
		    }
		  if (p->image_offset % 2 == 0)
		    {
		      if (++p->image_x >= p->image_width
			  && increment_image_y (p) < 0)
			return FALSE;
		    }
		}
	      break;

	    default:
	      goto bad_depth;
	    }
	  break;

	default:
	  fprintf (stderr, "preview.input_available: bad frame format %d\n",
		   p->params.format);
	  scan_done (p);
	  return FALSE;
	}
      if (p->input_tag < 0)
	{
	  display_maybe (p);
	  while (gtk_events_pending ())
	  {
	       gtk_widget_queue_draw(p->window);
	       gtk_main_iteration ();
	  }
	}
    }
  display_maybe (p);
  p->scanning = FALSE;
  return TRUE;

bad_depth:
  {
    char errbuf[100];

    snprintf (errbuf, sizeof (errbuf), "Preview cannot handle depth %d.",
	      p->params.depth);
    gsg_error (errbuf);
  }
  scan_done (p);
  return FALSE;
}

static void
scan_done (Preview * p)
{
  int i;

  p->scanning = FALSE;
  if (p->input_tag >= 0)
    {
      g_source_remove (p->input_tag);
      p->input_tag = -1;
    }
  sane_cancel (p->dialog->dev);

  restore_option (p, p->dialog->well_known.dpi,
		  p->saved_dpi, p->saved_dpi_valid);
  for (i = 0; i < 4; ++i)
    restore_option (p, p->dialog->well_known.coord[i],
		    p->saved_coord[i], p->saved_coord_valid[i]);
  set_option_bool (p, p->dialog->well_known.preview, SANE_FALSE);

  gtk_widget_set_sensitive (p->cancel, FALSE);
  gtk_widget_set_sensitive (p->preview, TRUE);
  gsg_set_sensitivity (p->dialog, TRUE);
  // gtk_widget_set_sensitive (p->dialog->window->parent->parent->parent, TRUE);
}

static void
scan_start (Preview * p)
{
  SANE_Handle dev = p->dialog->dev;
  SANE_Status status;
  char buf[256];
  int fd;

  p->scanning = TRUE;
  gtk_widget_set_sensitive (p->cancel, TRUE);
  gtk_widget_set_sensitive (p->preview, FALSE);
  gsg_set_sensitivity (p->dialog, FALSE);
  // gtk_widget_set_sensitive (p->dialog->window->parent->parent->parent, FALSE);

  if (p->preview_data == NULL)
      p->preview_data = malloc (p->image_width * p->image_height * 3);
  else
      p->preview_data = realloc (p->preview_data, 3 * p->image_width * p->image_height);
  /* clear old preview: */
  memset (p->preview_data, 0xff, 3 * p->image_width * p->image_height);
  gtk_widget_queue_draw (p->window);

  if (p->input_tag >= 0)
    {
      g_source_remove (p->input_tag);
      p->input_tag = -1;
    }

  gsg_sync (p->dialog);

  status = sane_start (dev);

#ifdef SANE_STATUS_WARMING_UP
  Progress_t *progress;

  if (status == SANE_STATUS_WARMING_UP)
    {
      snprintf (buf, sizeof (buf), "Scanner is warming up.");
      progress =
	progress_new ("Warming up ...", buf, GTK_WINDOW(p->top), NULL, 0);

#  ifdef HAVE_SYS_TIME_H
      struct timeval start, current;
      gettimeofday (&start, NULL);
#  endif

      while (status == SANE_STATUS_WARMING_UP)
	{

#  ifdef HAVE_SYS_TIME_H
	  gettimeofday (&current, NULL);
	  /* we assume that warming up won't exceed 60 seconds */
	  progress_update (progress,
			   (current.tv_sec - start.tv_sec) / (gfloat) 60);
#  endif

	  while (gtk_events_pending ())
	  {
	      gtk_widget_queue_draw(p->window);
	      gtk_main_iteration ();
          }
	  status = sane_start (dev);
	}
      progress_free (progress);
    }
#endif /* SANE_STATUS_WARMING_UP */
  if (status != SANE_STATUS_GOOD)
    {
      snprintf (buf, sizeof (buf),
		"Failed to start scanner: %s.", sane_strstatus (status));
      gsg_error (buf);
      scan_done (p);
      return;
    }

  status = sane_get_parameters (dev, &p->params);
  if (status != SANE_STATUS_GOOD)
    {
      snprintf (buf, sizeof (buf),
		"Failed to obtain parameters: %s.", sane_strstatus (status));
      gsg_error (buf);
      scan_done (p);
      return;
    }

  if ((p->params.format >= SANE_FRAME_RGB &&
       p->params.format <= SANE_FRAME_BLUE) &&
      p->params.depth == 1 && p->params.pixels_per_line % 8 != 0)
    {
      snprintf (buf, sizeof (buf),
		"Can't handle unaligned 1 bit RGB or three-pass mode.");
      gsg_error (buf);
      scan_done (p);
      return;
    }

  p->image_offset = p->image_x = p->image_y = 0;

  if (p->params.format >= SANE_FRAME_RED
      && p->params.format <= SANE_FRAME_BLUE)
    p->image_offset = p->params.format - SANE_FRAME_RED;

  if (!p->image_data || p->params.pixels_per_line != p->image_width
      || (p->params.lines >= 0 && p->params.lines != p->image_height))
    {
      /* image size changed */
      if (p->image_data)
	free (p->image_data);

      p->image_width = p->params.pixels_per_line;
      p->image_height = p->params.lines;
      if (p->image_height < 0)
	p->image_height = 32;	/* may have to adjust as we go... */

      p->image_data = malloc (p->image_width * p->image_height * 3);
      p->preview_data = malloc (p->preview_width * p->image_height * 3);
      if (!p->image_data)
	{
	  snprintf (buf, sizeof (buf),
		    "Failed to allocate image memory: %s.", strerror (errno));
	  gsg_error (buf);
	  scan_done (p);
	  return;
	}
      memset (p->image_data, 0xff, 3 * p->image_width * p->image_height);
    }

  if (p->selection.active)
    {
      p->previous_selection = p->selection;
      p->selection.active = FALSE;
      gtk_widget_queue_draw (p->window);
    }
  p->scanning = TRUE;

  if (sane_set_io_mode (dev, SANE_TRUE) == SANE_STATUS_GOOD
      && sane_get_select_fd (dev, &fd) == SANE_STATUS_GOOD)
  {
    GIOChannel *chan = g_io_channel_unix_new (fd);
    p->input_tag =
      g_io_add_watch (chan, G_IO_IN | G_IO_HUP | G_IO_ERR | G_IO_PRI,
		      (GIOFunc) input_available, p);
    g_io_channel_unref(chan);
  }
  else
    input_available (-1, G_IO_IN, p);
}

static void
establish_selection (Preview * p)
{
  float min, max, normal, dev_selection[4];
  int i;

  memcpy (dev_selection, p->surface, sizeof (dev_selection));
  if (p->selection.active)
    for (i = 0; i < 2; ++i)
      {
	min = p->surface[i];
	if (min <= -INF)
	  min = 0.0;
	max = p->surface[i + 2];
	if (max >= INF)
	  max = p->preview_width;

	normal =
	  1.0 / (((i == 0) ? p->preview_width : p->preview_height) - 1);
	normal *= (max - min);
	dev_selection[i] = p->selection.coord[i] * normal + min;
	dev_selection[i + 2] = p->selection.coord[i + 2] * normal + min;
      }
  for (i = 0; i < 4; ++i)
    set_option_float (p, p->dialog->well_known.coord[i], dev_selection[i]);
  gsg_update_scan_window (p->dialog);
  if (p->dialog->param_change_callback)
    (*p->dialog->param_change_callback) (p->dialog,
					 p->dialog->param_change_arg);
}

static int
make_preview_image_path (Preview * p, size_t filename_size, char *filename)
{
  return gsg_make_path (filename_size, filename, 0, "preview-",
			p->dialog->dev_name, ".ppm");
}

static void
restore_preview_image (Preview * p)
{
  u_int psurface_type, psurface_unit;
  char filename[PATH_MAX];
  int width, height;
  float psurface[4];
  size_t nread;
  FILE *in;

  /* See whether there is a saved preview and load it if present: */

  if (make_preview_image_path (p, sizeof (filename), filename) < 0)
    return;

  in = fopen (filename, "r");
  if (!in)
    return;

  /* Be careful about consuming too many bytes after the final newline
     (e.g., consider an image whose first image byte is 13 (`\r').  */
  if (fscanf (in, "P6\n# surface: %g %g %g %g %u %u\n%d %d\n255%*[\n]",
	      psurface + 0, psurface + 1, psurface + 2, psurface + 3,
	      &psurface_type, &psurface_unit, &width, &height) != 8)
    return;

  if (GROSSLY_DIFFERENT (psurface[0], p->surface[0])
      || GROSSLY_DIFFERENT (psurface[1], p->surface[1])
      || GROSSLY_DIFFERENT (psurface[2], p->surface[2])
      || GROSSLY_DIFFERENT (psurface[3], p->surface[3])
      || psurface_type != p->surface_type || psurface_unit != p->surface_unit)
    /* ignore preview image that was acquired for/with a different surface */
    return;

  p->image_width = width;
  p->image_height = height;
  if ((width == 0) || (height == 0))
    return;
  p->image_data = malloc (3 * width * height);
  p->preview_data = malloc (3 * width * height);
  if (!p->image_data)
    return;

  nread = fread (p->image_data, 3, width * height, in);

  p->image_y = nread / width;
  p->image_x = nread % width;
}

/* This is executed _after_ the gtkpreview's expose routine.  */
static gint
expose_handler (GtkWidget * window, cairo_t *cr, gpointer data)
{
  Preview *p = data;

  if (p->preview_data == NULL) return FALSE;

  GdkPixbuf* pixbuf = gdk_pixbuf_new_from_data(p->preview_data, GDK_COLORSPACE_RGB, FALSE, 8, p->preview_width,
                                               p->preview_height, p->preview_width * 3, NULL/*preview_pixbuf_data_destroy*/, NULL);
  gdk_cairo_set_source_pixbuf (cr, pixbuf, 0, 0);
  cairo_paint(cr);
  g_object_unref(pixbuf);

  if (p->selection_drag == FALSE)
  	update_selection (p);
  draw_selection (p, cr);

  p->selection.active = FALSE;
  return FALSE;
}

static gboolean
button_press_handler(GtkWidget *window,
                                     GdkEvent *event, gpointer data)
{
    Preview *p = data;
    if (p->scanning)
	  return FALSE;
	p->selection.coord[0] = event->button.x;
	p->selection.coord[1] = event->button.y;
	p->selection_drag = TRUE;
    return FALSE;
}

static gboolean
button_release_handler (GtkWidget *window,
                                         GdkEvent *event, gpointer data)
{
    Preview *p = data;
    int i, tmp;

    if (p->scanning == TRUE)
	  return FALSE;
    if (p->selection_drag == FALSE)
	  return FALSE;
	p->selection_drag = FALSE;

	p->selection.coord[2] = event->button.x;
	p->selection.coord[3] = event->button.y;
	p->selection.active =
	  (p->selection.coord[0] != p->selection.coord[2]
	   || p->selection.coord[1] != p->selection.coord[3]);

	if (p->selection.active)
	  {
	    for (i = 0; i < 2; i += 1)
	      if (p->selection.coord[i] > p->selection.coord[i + 2])
		{
		  tmp = p->selection.coord[i];
		  p->selection.coord[i] = p->selection.coord[i + 2];
		  p->selection.coord[i + 2] = tmp;
		}
	    if (p->selection.coord[0] < 0)
	      p->selection.coord[0] = 0;
	    if (p->selection.coord[1] < 0)
	      p->selection.coord[1] = 0;
	    if (p->selection.coord[2] >= p->preview_width)
	      p->selection.coord[2] = p->preview_width - 1;
	    if (p->selection.coord[3] >= p->preview_height)
	      p->selection.coord[3] = p->preview_height - 1;
	  }
	establish_selection (p);
	gtk_widget_queue_draw (p->window);
    return FALSE;
}

static gboolean
motion_handler (GtkWidget *window,
                            GdkEvent *event, gpointer data)
{
    Preview *p = data;
    if (p->scanning == TRUE) return FALSE;
	if (p->selection_drag == TRUE)
	  {
	    p->selection.active = TRUE;
	    p->selection.coord[2] = event->motion.x;
	    p->selection.coord[3] = event->motion.y;
	    gtk_widget_queue_draw (p->window);
	  }
    return FALSE;
}

static void
start_button_clicked (GtkWidget * widget, gpointer data)
{
  preview_scan (data);
}

static void
cancel_button_clicked (GtkWidget * widget, gpointer data)
{
  scan_done (data);
}

static void
top_destroyed (GtkWidget * widget, gpointer call_data)
{
  Preview *p = call_data;

  p->top = NULL;
}

Preview *
preview_new (GSGDialog * dialog)
{
  static int first_time = 1;
  GtkWidget *table, *frame;
  GtkBox *vbox;
  Preview *p;

  p = malloc (sizeof (*p));
  if (!p)
    return 0;
  memset (p, 0, sizeof (*p));

  p->dialog = dialog;
  p->surface_unit = SANE_UNIT_MM;
  p->input_tag = -1;
  p->image_data = NULL;
  p->preview_data = NULL;
  p->preview_row = NULL;
  p->top = NULL;
  p->scanning = FALSE;

  if (first_time)
    {
      first_time = 0;
      // gtk_preview_set_gamma (preferences.preview_gamma);
      // gtk_preview_set_install_cmap (preferences.preview_own_cmap);
    }

  p->top = gtk_dialog_new ();
  g_signal_connect (G_OBJECT (p->top), "destroy",
		      G_CALLBACK (top_destroyed), p);
  gtk_window_set_title (GTK_WINDOW (p->top), "xscanimage preview");
  vbox = GTK_BOX (gtk_dialog_get_content_area(GTK_DIALOG (p->top)));

  // construct the preview area (table with sliders & preview window)
  table = gtk_grid_new ();
  gtk_grid_set_column_spacing (GTK_GRID (table), 1);
  gtk_grid_set_row_spacing (GTK_GRID (table), 1);
  gtk_container_set_border_width (GTK_CONTAINER (table), 2);
  gtk_box_pack_start (vbox, table,  TRUE,  TRUE, 0);

  /* the empty box in the top-left corner */
  frame = gtk_frame_new ( /* label */ 0);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
  gtk_grid_attach (GTK_GRID (table), frame, 0, 0, 1, 1);

  /* the horizontal ruler */
  p->hruler = gtk3_ruler_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (table), p->hruler, 1, 0, 1, 1);
  gsg_widget_placement(GTK_WIDGET (p->hruler), GTK_ALIGN_FILL, GTK_ALIGN_CENTER, 0, 0, 0, 0);

  /* the vertical ruler */
  p->vruler = gtk3_ruler_new (GTK_ORIENTATION_VERTICAL);
  gtk_grid_attach (GTK_GRID (table), p->vruler, 0, 1, 1, 1);
  gsg_widget_placement(GTK_WIDGET (p->vruler), GTK_ALIGN_CENTER, GTK_ALIGN_FILL, 0, 0, 0, 0);

  /* the preview area */
  p->window = gtk_drawing_area_new ();
  gtk_widget_set_app_paintable(p->window, TRUE);
  gsg_widget_placement(GTK_WIDGET (p->window), GTK_ALIGN_FILL, GTK_ALIGN_FILL, 0, 0, 0, 0);

  gtk_widget_set_events (p->window,
			 GDK_EXPOSURE_MASK |
			 GDK_POINTER_MOTION_MASK |
			 GDK_POINTER_MOTION_HINT_MASK |
			 GDK_BUTTON_PRESS_MASK |
			 GDK_BUTTON_RELEASE_MASK);

  g_signal_connect_after (G_OBJECT (p->window), "draw",
			    G_CALLBACK(expose_handler), p);

  g_signal_connect(G_OBJECT(p->window), "button_press_event",
                 G_CALLBACK(button_press_handler), p);
  g_signal_connect(G_OBJECT(p->window), "motion_notify_event",
                 G_CALLBACK(motion_handler), p);
  g_signal_connect(G_OBJECT(p->window), "button_release_event",
                 G_CALLBACK(button_release_handler), p);
  g_signal_connect (p->window ,"configure-event",
                 G_CALLBACK(preview_area_resize), p);

  gtk3_ruler_add_track_widget (GTK3_RULER (p->hruler), p->window);
  gtk3_ruler_add_track_widget (GTK3_RULER (p->vruler), p->window);

  p->viewport = gtk_frame_new ( 0);
  gtk_frame_set_shadow_type (GTK_FRAME (p->viewport), GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (p->viewport), p->window);

  gtk_grid_attach (GTK_GRID (table), p->viewport, 1, 1, 1, 1);
  gsg_widget_placement(GTK_WIDGET (p->viewport), GTK_ALIGN_FILL, GTK_ALIGN_FILL, 0, 0, 0, 0);
  gtk_widget_set_vexpand (GTK_WIDGET (p->viewport),TRUE);
  gtk_widget_set_hexpand (GTK_WIDGET (p->viewport),TRUE);

  preview_update (p);

  /* fill in action area: */

  /* Preview button */
  p->preview = gtk_dialog_add_button (GTK_DIALOG (p->top), "Acquire Preview",GTK_RESPONSE_OK);
  g_signal_connect (G_OBJECT (p->preview), "clicked",
		      G_CALLBACK (start_button_clicked), p);
  gtk_widget_show (p->preview);

  /* Cancel button */
  p->cancel = gtk_dialog_add_button (GTK_DIALOG (p->top), "Cancel Preview", GTK_RESPONSE_CANCEL);
  g_signal_connect (G_OBJECT (p->cancel), "clicked",
		      G_CALLBACK (cancel_button_clicked), p);
  gtk_widget_set_sensitive (p->cancel, FALSE);

  gtk_widget_show (p->cancel);
  gtk_widget_show (p->viewport);
  gtk_widget_show (p->window);
  gtk_widget_show (p->hruler);
  gtk_widget_show (p->vruler);
  gtk_widget_show (frame);
  gtk_widget_show (table);
  gtk_widget_show (p->top);

  return p;
}

void
preview_update (Preview * p)
{
  float val, width, height, max_width, max_height;
  const SANE_Option_Descriptor *opt;
  int i, surface_changed;
  SANE_Value_Type type;
  SANE_Unit unit;
  float min, max;

  surface_changed = 0;
  unit = SANE_UNIT_PIXEL;
  type = SANE_TYPE_INT;
  for (i = 0; i < 4; ++i)
    {
      val = (i & 2) ? INF : -INF;
      if (p->dialog->well_known.coord[i] > 0)
	{
	  opt = sane_get_option_descriptor (p->dialog->dev,
					    p->dialog->well_known.coord[i]);
	  assert (opt->unit == SANE_UNIT_PIXEL || opt->unit == SANE_UNIT_MM);
	  unit = opt->unit;
	  type = opt->type;

	  get_bounds (opt, &min, &max);
	  if (i & 2)
	    val = max;
	  else
	    val = min;
	}
      if (p->surface[i] != val)
	{
	  surface_changed = 1;
	  p->surface[i] = val;
	}
    }
  if (p->surface_unit != unit)
    {
      surface_changed = 1;
      p->surface_unit = unit;
    }
  if (p->surface_type != type)
    {
      surface_changed = 1;
      p->surface_type = type;
    }
  if (surface_changed && p->image_data)
    {
      free (p->image_data);
      free (p->preview_data);
      p->image_data = 0;
      p->preview_data = 0;
      p->image_width = 0;
      p->image_height = 0;
    }

  /* guess the initial preview window size: */

  width = p->surface[GSG_BR_X] - p->surface[GSG_TL_X];
  height = p->surface[GSG_BR_Y] - p->surface[GSG_TL_Y];
  if (p->surface_type == SANE_TYPE_INT)
    {
      width += 1.0;
      height += 1.0;
    }
  else
    {
      width += SANE_UNFIX (1.0);
      height += SANE_UNFIX (1.0);
    }

  assert (width > 0.0 && height > 0.0);

  if (width >= INF || height >= INF)
    p->aspect = 1.0;
  else
    p->aspect = width / height;

  if (surface_changed)
    {
      int width, height;
       screen_size_get_dimensions (&width, &height);
      max_width = 0.5 * (double)width;
      max_height = 0.5 * (double)height;
    }
  else
    {
      GtkAllocation alloc;
      gtk_widget_get_allocated_size (p->window, &alloc, NULL);
      max_width = alloc.width;
      max_height = alloc.height;
    }

  if (p->surface_unit != SANE_UNIT_PIXEL)
    {
      width = max_width;
      height = width / p->aspect;

      if (height > max_height)
	{
	  height = max_height;
	  width = height * p->aspect;
	}
    }
  else
    {
      if (width > max_width)
	width = max_width;

      if (height > max_height)
	height = max_height;
    }

  /* re-adjust so we maintain aspect without exceeding max size: */
  if (width / height != p->aspect)
    {
      if (p->aspect > 1.0)
	height = width / p->aspect;
      else
	width = height * p->aspect;
    }

  p->preview_width = width + 0.5;
  p->preview_height = height + 0.5;
  if (surface_changed)
    {
      gtk_widget_set_size_request (GTK_WIDGET (p->window),
			    p->preview_width, p->preview_height);
      if (gtk_widget_is_drawable(GTK_WIDGET(p->window)))
	preview_area_resize (p->window, NULL, p);

      if (preferences.preserve_preview)
	restore_preview_image (p);
    }
  update_selection (p);
  gtk_widget_queue_draw (p->window);
}

void
preview_scan (Preview * p)
{
  float min, max, swidth, sheight, width, height, dpi = 0;
  const SANE_Option_Descriptor *opt;
  gint gwidth, gheight;
  int i;
  SANE_Status status;


  save_option (p, p->dialog->well_known.dpi,
	       &p->saved_dpi, &p->saved_dpi_valid);
  for (i = 0; i < 4; ++i)
    save_option (p, p->dialog->well_known.coord[i],
		 &p->saved_coord[i], p->saved_coord_valid + i);

  /* determine dpi, if necessary: */

  if (p->dialog->well_known.dpi > 0)
    {
      opt = sane_get_option_descriptor (p->dialog->dev,
					p->dialog->well_known.dpi);

      gwidth = p->preview_width;
      gheight = p->preview_height;

      height = gheight;
      width = height * p->aspect;
      if (width > gwidth)
	{
	  width = gwidth;
	  height = width / p->aspect;
	}

      swidth = (p->surface[GSG_BR_X] - p->surface[GSG_TL_X]);
      if (swidth < INF)
	dpi = MM_PER_INCH * width / swidth;
      else
	{
	  sheight = (p->surface[GSG_BR_Y] - p->surface[GSG_TL_Y]);
	  if (sheight < INF)
	    dpi = MM_PER_INCH * height / sheight;
	  else
	    dpi = 18.0;
	}
      get_bounds (opt, &min, &max);
      if (dpi < min)
	dpi = min;
      if (dpi > max)
	dpi = max;
      status = set_option_float (p, p->dialog->well_known.dpi, dpi);
      if (status != SANE_STATUS_GOOD)
	return;
    }

  /* set the scan window (necessary since backends may default to
     non-maximum size):  */
  for (i = 0; i < 4; ++i)
    set_option_float (p, p->dialog->well_known.coord[i], p->surface[i]);
  set_option_bool (p, p->dialog->well_known.preview, SANE_TRUE);

  /* OK, all set to go */
  scan_start (p);
}

void
preview_destroy (Preview * p)
{
  char filename[PATH_MAX];
  FILE *out;

  if (p->scanning)
    scan_done (p);		/* don't save partial window */
  else if (preferences.preserve_preview && p->image_data
	   && make_preview_image_path (p, sizeof (filename), filename) >= 0)
    {
      /* save preview image */
      out = fopen (filename, "w");
      if (out)
	{
	  /* always save it as a PPM image: */
	  fprintf (out, "P6\n# surface: %g %g %g %g %u %u\n%d %d\n255\n",
		   p->surface[0], p->surface[1], p->surface[2], p->surface[3],
		   p->surface_type, p->surface_unit,
		   p->image_width, p->image_height);
	  fwrite (p->image_data, 3, p->image_width * p->image_height, out);
	  fclose (out);
	}
    }
  if (p->image_data)
    free (p->image_data);
  p->image_data = NULL;
  if (p->preview_data)
    free (p->preview_data);
  p->preview_data = NULL;
  if (p->preview_row)
    free (p->preview_row);
  p->preview_row = NULL;
  if (p->top)
    gtk_widget_destroy (p->top);
  free (p);
}
